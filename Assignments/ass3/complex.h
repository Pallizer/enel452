/**
   Declare a complex type.
*/
struct Complex {
    double re;
    double im;
};
