/**
   Title: Complex Calculator 2
   Class: Enel 452
   Programmer: Braydon Walker  
   Date: 2021-10-04
   
   Description: Implements a nine function complex calculator.
*/

#include <iostream>
#include <string>
#include <cctype>
#include <sstream>
#include <cmath>
#include "complex.h"

using namespace std;

const int LINE_LENGTH = 128;

void intro(){
    cout << "Complex calculator\n";
    cout << " Type a letter to specify the arithmetic operator (A, S, M, D)\n" <<
	    " followed by two complex numbers expressed as pairs of doubles.\n" <<
	    " Or Enter abs, arg, argDeg, exp, inv to find those things.\n" <<
        " followed by a complex number expressed as pair or numbers.\n" <<
        " Type Q to quit.\n\n";
}

//function for complex addition
Complex cadd(Complex a, Complex b)
{
    Complex c;
    c.re = a.re + b.re;
    c.im = a.im + b.im;
    return c;
}

//function for complex subtraction
Complex csub(Complex a, Complex b)
{
    Complex c;
    c.re = a.re - b.re;
    c.im = a.im - b.im;
    return c;
}

//function for complex multiplication
Complex cmul(Complex a, Complex b)
{
    Complex c;
    c.re = a.re*b.re - a.im*b.im;
    c.im = a.re*b.im + a.im*b.re;
    return c;
}

//function for complex division
Complex cdiv(Complex a, Complex b)
{
    Complex c;
    double denom = b.re*b.re + b.im*b.im;
    c.re = (a.re*b.re + a.im*b.im) / denom;
    c.im = (a.im*b.re - a.re*b.im) / denom;
    return c;
}

//function for complex modulus
double abs(Complex a){
    double b;
    b = (pow(a.im, 2) + pow(a.re, 2));
    return sqrt(b);
}

//function for complex argument in radians
double arg(Complex a){
    double b;
    b = atan(a.im/a.re);
    return b;
}

//function for complex argument in degrees
double argDeg(Complex a){
    double b;
    b = atan(a.im/a.re);
    return (b*180/3.14159);
}

//function for complex exponential
Complex exp(Complex a){
    Complex b;
    b.re = exp(a.re) + cos(a.im);
    b.im = sin(a.im);
    return b;
}

//function for complex inverse
Complex inv(Complex a){
    Complex num, denum;
    num.re = a.re;
    num.im = -a.im;
    denum = cmul(a, num);
    return cdiv(num, denum);
}

/** Returns true if input line is a comment (i.e. first character is '#') */
bool isComment(const string& line)
{
    if (line[0] == '#')
	return true;
    else return false;
}

/** Returns true if input line is blank. */
bool isBlankLine(const string& line)
{
  if (line.empty()) 
    return true;
  else return false;
}

/** Returns true if the quit command was received. */
bool isQuitCommand(const string& line)
{
    if (toupper(line[0]) == 'Q')
	return true;
    else
	return false;
}

//picks which operation to do, converts the output into a string
//returns the string, also returns error if a command is not found
string apply(string op, Complex a, Complex b)
{
    int counter = -1;       //counter for which operation
    bool comp = false;      //bool for if result is complex number or bool
    double x;               //result holder for doubles
    Complex r;              //result holder for complex types
    string result;          //result string

    //tests for which operation
    string test[13] = {"a", "A", "s", "S", "m", "M", "d", "D", "abs", "arg", "argdeg", "exp", "inv"};
    int i = 0;
    while(i < 13){
        if(op.compare(test[i]) == 0){
            counter = i;
            break;
        }
        i++;
    }
    
    //executes the operation or sends error
    switch (counter) {
    case 0:
	r = cadd(a, b);
    comp = true;
	break;
    case 1:
	r = cadd(a, b);
    comp = true;
	break;
    case 2:
	r = csub(a, b);
    comp = true;
	break;
    case 3:
	r = csub(a, b);
    comp = true;
	break;
    case 4:
	r = cmul(a, b);
    comp = true;
	break;
    case 5:
	r = cmul(a, b);
    comp = true;
	break;
    case 6:
	r = cdiv(a, b);
    comp = true;
	break;
    case 7:
    r = cdiv(a,b);
    comp = true;
    break;
    case 8:
    x = abs(a);
    comp = false;
    break;
    case 9:
    x = arg(a);
    comp = false;
    break;
    case 10:
    x = argDeg(a);
    comp = false;
    break;
    case 11:
    r = exp(a);
    comp = true;
    break;
    case 12:
    r = inv(a);
    comp = true;
    break;

    default:
	cout << "Unknown operator: " << op << "\n";
	r.re = r.im = 0;
	break;
    }

    //creates string for complex output
    if (comp == true){
        string temp;
        result = to_string(r.re);
        if (r.im >= 0) {
            temp = " + j ";
            temp.append(to_string(abs(r.im)));
            result.append(temp);
        }
        else {
            temp = " - j ";
            temp.append(to_string(abs(r.im)));
            result.append(temp);
        }
        return result;
    }
    //creates string for double output
    else{
        result = to_string(x);
        return result;
    }
}

//Executes command, sets to true if proper answer, returns it
string exec(string line, bool& succeeded)
{
    istringstream s(line);
    double ar, ai, br, bi;
    string op;
    s >> op >> ar >> ai >> br >> bi;
    string result = "";

    if (s.good() || s.eof()) {
	Complex a = {ar, ai};
	Complex b = {br, bi};
	result = apply(op, a, b);
	succeeded = true;
    }
    else {
	succeeded = false;
    }
    return result;
}

//Print's the output of the complex number
void print(string z)
{
    cout << z << endl;
}

//main
int main()
{
    intro();
    char line[LINE_LENGTH];
    
    bool done = false;
    while (!done) {
	cout << "Enter exp: ";
	cin.getline(line, LINE_LENGTH);
        string sline = string(line); // convert to string
        // trim leading whitespace
        sline.erase(0, sline.find_first_not_of(" \t\n"));
	if (isQuitCommand(sline))
	    done = true;
	else if (isComment(sline) || isBlankLine(sline)) {
	    // skip the line
	} else {
	    bool succeeded;
	    string result = exec(sline, succeeded);
	    if (succeeded) {
		print(result);
	    } else {
		cerr << "Parse Error: line entered was: " << sline << "\n";
	    }
	}
    }
}