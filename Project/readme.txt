KEY FOR VISUALS

x  | red in any lane

-> | advanced green turn in south bound lane | green in east bound lane

>  | advanced yellow turn in south bound lane | yellow in east bound lane

<- | advanced green turn in north bound lane | green in west bound lane

<  | advanced yellow turn in north bound lane | yellow in west bound lane

|  |
v  | advanced green turn in west bound lane | green in south bound lane

v  | advanced yellow turn in west bound lane | yellow in south bound lane

^  |
|  | advanced green turn in east bound lane | green in north bound lane

^  | advanced yellow turn in east bound lane | yellow in north bound lane


CONNCTING via PuTTY

open device manager and find ports
expand it and see which (COMx) your stm is connected to
open putty and select serial connection with a speed of 115200
type in the COMx we found earlier in the serial line option and click open
you should be connected to the stm via PuTTy


NOTES

I was unable to get Usart transmit to work, everytime i enabled it in cubeide and tried
using the function call HAL_UART_Receive_IT or HAL_UART_Receive it would say they were
undefined. It is my understanding that these HAL functions are supposed to be here for 
us to use but for some reason I was not able to use this one, and as a result i was 
unable to implement funcitons for signalling an emergency response and outputting
the emergency log